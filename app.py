from flask import Flask, render_template, g, redirect, url_for, request, session
import sqlite3
import os
from functools import wraps
app = Flask(__name__)
app.database='mangas.db'
app.secret_key='Z\xfc\x1ak\xa2A\x1b\xb6o\xd9\xeb\x143\xd8Y\x0e\xc6\xbf\x1e\xe0\xf0\xcc;\x91'
def login_required(test):
    @wraps(test)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return test(*args, **kwargs)
        else:
                return redirect(url_for('login'))
    return wrap
@app.route('/',methods=['GET','POST'])
@login_required
def home():
    g.db=connect_db()
    cursor=g.db.execute('select title,lastvol,nextvol,date from mangas')
    rows= [dict(title=row[0],lastvol=row[1],nextvol=row[2],date=row[3]) for row in cursor.fetchall()]
    g.db.close()
    return render_template('index.html',rows=rows)
@app.route('/login',methods=['GET','POST'])
def login():
    error=None
    if request.method == 'POST':
        if request.form['username'] != 'rdup' or request.form['password'] != 'gba182':
            error = 'Invalid credentials'
        else:
            session['logged_in']=True
            return redirect(url_for('home'))
    return render_template('login.html',error=error)
@app.route('/logout')
def logout():
    session.pop('logged_in')
    return(redirect(url_for('home')))
@app.route('/extra',methods=['POST'])
def extra():
    if request.method=='POST':
        g.db=connect_db()
        g.db.execute('UPDATE mangas set lastvol=lastvol+1 where title=?',[request.args.get('title')])
        g.db.commit()
        g.db.close()
        return redirect(url_for('home'))
    return render_template('index.html',rows=rows)
@app.route('/add',methods=['GET','POST'])
def add():
    error=None
    g.db=connect_db()
    cursor=g.db.execute('INSERT into mangas(title,url,lastvol,oldnextvol) VALUES(?,?,?,?)',[request.form['title'],request.form['url'],request.form['lastvol'],request.form['lastvol']])
    g.db.commit()
    g.db.close()
    return redirect(url_for('home'))
    #else:
    #    return 'fuck2'
@app.route('/del',methods=['POST'])
def delete():
    if request.method=='POST':
        g.db=connect_db()
        g.db.execute('DELETE from mangas where title=?',[request.args.get('title')])
        g.db.commit()
        g.db.close()
        return redirect(url_for('home'))
    
@app.route('/refresh',methods=['POST'])
def refresh():
    os.system("python scrap.py >/dev/null 2>&1")
    return redirect(url_for('home'))
def connect_db():
    return sqlite3.connect(app.database)
# start the server with the 'run()' method
if __name__ == '__main__':
    app.run()

